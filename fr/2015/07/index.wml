#use wml::debian::projectnews::header PUBDATE="2015-XX-XX" SUMMARY=""

# $Id$
# $Rev$
# Status: [open-for-edit]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

<intro issue="septième" />
<toc-display/>


<toc-add-entry name="XXX">foo</toc-add-entry>

<p>
More details about foo.
</p>

<toc-add-entry name=""></toc-add-entry>

<toc-add-entry name="os_award">Stefano Zacchiroli reçoit le prix Open Source O'Reilly</toc-add-entry>

Stefano Zacchiroli a reçu un
<a href="https://en.wikipedia.org/wiki/O%27Reilly_Open_Source_Award#2015">prix Open Source O'Reilly
</a>
à l'OSCON (O'Reilly Open Source Convention) pour ses contributions à
Debian et à la communauté du logiciel libre. Stefano a assumé la
responsabilité de chef du projet Debian (DPL) pendant trois ans.
Il appartient actuellement au bureau de l'OSI (Open Source Initiative) et
il est chercheur à l'IRILL à Paris.

<toc-add-entry name="debinst">Premières versions Alpha de l'installateur pour Debian Stretch </toc-add-entry>

<p>
Cyril Brulebois
<a href="https://lists.debian.org/debian-devel-announce/2015/07/msg00005.html">a announcé</a>
la publication de la première version Alpha de l'installateur de Debian 9
<q>Stretch</q> puis de la seconde en direct de la fête anniversaire de
DebConf à Heidelberg, en Allemagne.
Il a envoyé <a href="http://mraw.org/blog/2015/07/22/D-I_Stretch_Alpha_1/">sur son blog</a>
un rapide résumé des débuts du cycle de développement de l'installateur
pour Stretch, et
<a href="https://lists.debian.org/debian-devel-announce/2015/08/msg00004.html">a annoncé </a>
que la migration dans testing des paquets produisant les paquets spéciaux
<tt>udeb</tt>, utilisés par l'installateur seraient gelés juste avant la
publication d'une nouvelle version de l'installateur. Il appelle également
les mainteneurs de paquets avec de potentielles modifications pour
l'installateur à se coordonner à travers la liste de diffusion
<a href="https://lists.debian.org/debian-boot">debian-boot</a>.
</p>

<toc-add-entry name="bye-sparc">L'architecture SPARC retirée de l'archive</toc-add-entry>

<p>
Joerg Jaspert <a href="https://lists.debian.org/debian-devel-announce/2015/07/msg00006.html">a annoncé</a>
que la prise en charge de l'<a href="$(HOME)/ports/sparc/">architecture SPARC</a>
a été retirée de l'archive officielle. Cette architecture a été introduite
dans <q>Slink</q>, la version 2.1 de Debian.
Andrew Carter partage <a href="http://flosslinuxblog.blogspot.de/2015/07/bye-sparc-for-now.html">ses souvenirs</a>
sur SPARC dans Debian.
</p>

<toc-add-entry name="uefi">La nouvelle équipe UEFI de Debian</toc-add-entry>

<p>
Steve McIntyre <a href="http://blog.einval.com/2015/08/02#new_debian_uefi_team">a annoncé</a>
sur son blog la création d'une <a
href="https://alioth.debian.org/projects/uefi/">équipe UEFI</a> dans
Debian, utilisant le canal IRC <tt>#debian-uefi</tt> récemment ouvert sur
<tt>irc.debian.org</tt>. L'équipe est ouverte à de nouveaux membres
intéressés à contribuer à ces paquets et à UEFI en général.
Steve a aussi rendu compte du lancement d'une
<a href="ttp://blog.einval.com/2015/08/02#tracking_broken_UEFI_implementations">initiative
inter-distribution</a> pour suivre les implémentations cassées d'UEFI.
Si vous avez une aventure épouvantable particulière avec UEFI, décrivez la
en détail sur
<a href="http://wiki.osdev.org/Broken_UEFI_implementations">la page dédiée du wiki</a>.
<p>

<toc-add-entry name="freedombox">Publication de FreedomBox 0.5</toc-add-entry>

<p>
James Valleroy
<a href="https://lists.alioth.debian.org/pipermail/freedombox-discuss/2015-August/006831.html">a announcé</a>
la publication de la version 0.5 de FreedomBox. Le
<a href="https://freedomboxfoundation.org/">projet FreedomBox</a>
est un projet communautaire pour développer, concevoir et promouvoir des
serveurs personnels basé sur Debian faisant tourner des logiciels libres
pour les communications privées et confidentielles.
Davantage d'information sur cette version est disponible dans les
<a href="https://wiki.debian.org/FreedomBox/ReleaseNotes">notes de publication</a>.
</p>

<p>Ritesh Raj Sarraf
<a href="http://www.researchut.com/blog/linux-containers-with-systemd">évoque sur son blog</a>
quelques uns des sujets discutés à la réunion des développeurs Debian sur
la compilation croisée, les licences du point de vue des utilisateurs
finaux et SystemD. Les groupes conteneurs ont pris une bonne partie du
temps de discussion compte tenu de la manière dont ils fonctionnent dans
systemd. Il fait part d'un exemple d'utilisation de systemd-nspawn à la
place de <q>Linux Containers</q> (LXC).</p>

<toc-add-entry name="Reports">Comptes-rendus</toc-add-entry>

<p>
Simon Kainz
<a href="http://blog.familiekainz.at/duck-challenge-at-debconf15.html">annonce</a>
et suit le concours hebdomadaire <a href="http://duck.debian.net/">DUCK</a>
pour aider à trouver, corriger et envoyer des paquets signalés par DUCK
avant la fin de DebConf15.
Prix, gagnants et scores pour
<a href="http://blog.familiekainz.at/duck-challenge-week-1.html">la première semaine</a> :
10 corrections et envois ;
<a href="http://blog.familiekainz.at/duck-challenge-week-2.html">la deuxième semaine</a> :
15 corrections et envois ;
<a href="http://blog.familiekainz.at/duck-challenge-week-3.html">la troisième semaine</a> :
10 corrections et envois, avec un total intermédiaire impressionnant de 35
paquets singalés par 25 personnes différentes.
<a href="http://blog.familiekainz.at/duck-challenge-week-4.html">Pour la quatrième semaine</a> :
14 corrections et envois .
</p>

<toc-add-entry name="TT">Trucs et astuces</toc-add-entry>

<p>
Plusieurs membres de la communauté Debian souhaitent partager certains
conseils : Christoph Egger montre comment
<a href="http://www.christoph-egger.org/weblog/entry/50">exporter des notes Org</a>
de emacs à html.
Petter Reinholdtsen explique comment
<a href="http://people.skolelinux.org/pere/blog/Typesetting_DocBook_footnotes_as_endnotes_with_dblatex.html">composer
des notes de bas de page DocBook avec dblatex</a>. Jonathan McDowell 
<a href="http://www.earth.li/~noodles/blog/2015/07/recovering-dgn3500.html">récupère
un routeur Netgear DGN3500 grâce au JTAG</a>. Utilisation d'ebumeter pour
<a href="http://blog.sesse.net/blog/tech/2015-07-25-18-08_stream_audio_level_monitoring_with_ebumteer.html">surveiller
le niveau des flux audio</a> avec ce truc de Steinar H. Gunderson.
Dans le monde de l'impression 3D, Elena Grandi partage un
<a href="http://social.gl-como.it/display/3e3ce0df2355b11e6937f69900231262"><q>makefile</q>
pour les projets OpenSCAD</a>.
James McCoy développe une idée pour faciliter la
<a href="https://jamessan.com/~jamessan//posts/porterbox-logins/">connexion aux machines de portage</a>
en utilisant un script et des morceaux de ssh_config. Steinar Gunderson
porte un regard nouveau sur <a
href="http://blog.sesse.net/blog/tech/2015-07-26-12-52_diy_web_video_streaming.html">comment faire
soit même de la diffusion vidéo sur internet</a>. François Marier montre
comment <a
href="http://feeding.cloud.geek.nz/posts/setting-wifi-regulatory-domain-linux-openwrt/">configurer
un régulateur de domaine wifi</a> avec Linux et OpenWRT.
</p>

<toc-add-entry name="WDYD">DPN asks, What do you do?</toc-add-entry>
<p>
	<a href="https://www.debian.org/">Debian</a> is a large and global community of a lot of small actors, projects, and 
teams. This month as part of a special feature we'd like to share with you 
something about a project or a team that is working in Debian that you 
may not be aware of.
</p>
<p>
	We reached out to the XXX team with the question: <q>What do you do?</q>
</p>
<p>
XXX responds:
<q>
.
</q>
</p>
<p>
For more information or to contact the team, visit: xxxxx.d.o or via IRC in XXXXXX.
</p>


<toc-add-entry name="interviews">Interviews</toc-add-entry>

	<p>Since the last issue of the Debian Project News, 
 new issues of the <a href="https://wiki.debian.org/ThisWeekInDebian"><q>This week in
	Debian</q> podcast</a> have been published:  with
<a href="http://frostbitemedia.libsyn.com/this-week-in-debian-episode-XXX">XXX</a>, xxx; and with
<a href="http://frostbitemedia.libsyn.com/this-week-in-debian-episode-XXX">XXX</a>, xxx.
</p>

	<p>There have also been 
 further <q>People behind Debian</q> interviews: with
<a href="https://raphaelhertzog.com/.....">Firstname
Lastname</a> (role in Debian here), ; and with
<a href="https://raphaelhertzog.com/.....">Firstname
Lastname</a> (role in Debian here).
</p>

        <p>There have been                                                              
<q><a href="http://people.skolelinux.org/pere/blog/tags/intervju/">DebianEdu interviews</a><q>
with
<a href=""></a> (in English) and
<a href=""></a> (in Norwegian),
who all describe, among other things, how they got involved in Debian Edu and their views about it.
</p>


<toc-add-entry name="other">Other news</toc-add-entry>

<p>The xxth issue of the
<a href="https://lists.debian.org/debian-devel-announce/">miscellaneous news for developers</a> 
has been released and covers the following topics:</p>
<ul>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
</ul>

<toc-add-entry name="events">Upcoming events</toc-add-entry>
<p>There are several upcoming Debian-related events:</p>
<ul>
 <li><a href="$(HOME)/events/">xyz</a></li>
 <li>...</li>
</ul>
<p>
You can find more information about Debian-related events and talks
on the <a href="$(HOME)/events">events section</a> of the Debian web site,
or subscribe to one of our events mailing lists for different regions:
<a href="https://lists.debian.org/debian-events-eu">Europe</a>, 
<a href="https://lists.debian.org/debian-events-nl">Netherlands</a>, 
<a href="https://lists.debian.org/debian-events-ha">Hispanic America</a>, 
<a href="https://lists.debian.org/debian-events-na">North America</a>.
</p>

<p>Do you want to organise a Debian booth or a Debian install party?
Are you aware of other upcoming Debian-related events?
Have you delivered a Debian talk that you want to link on our
<a href="$(HOME)/events/talks">talks page</a>? 
Send an email to the <a href="mailto:events@debian.org">Debian Events Team</a>.
</p>

<toc-add-entry name="newcontributors">Nouveaux développeurs et mainteneurs</toc-add-entry>

	<p>
8 candidats ont été
<a href="https://nm.debian.org/public/nmlist#done">acceptés</a>
	comme développeurs Debian,
10 candidats ont été
<a href="https://lists.debian.org/debian-project/2015/07/msg00008.html">acceptés</a>
	comme mainteneurs Debian et
20 <a href="https://udd.debian.org/cgi-bin/new-maintainers.cgi">personnes</a>
	ont commencé à maintenir des paquets depuis la dernière édition des
	« Nouvelles du projet Debian ». Bienvenue à
#DDs
Carsten Leonhardt,
Gianfranco Costamagna,
Tianon Gravi,
Graham Inggs,
Iain R. Learmonth,
Ximin Luo,
Christian Kastner,
Laura Arjona Reina,
#DMs
Riley Baird,
Christopher Knadle,
Alex Muntada,
Benjamin Barenblat,
Johan Van de Wauw,
Jose Luis Rivero,
Lennart Weller,
Paul Novotny,
Robie Basak,
Senthil Kuramans S,
#DCs
Martin Michlmayr,
Paulo Roberto Alves de Oliveira,
Guillaume Turri,
Víctor Manuel Jáquez Leal,
Michael Grünewald,
Antti Järvinen,
Etienne Dublé,
Francois Lafont,
Sven Geggus,
Nick Daly,
Martin Wimpress,
Adilson dos Reis,
Alexander Sosna,
Ivan Udovichenko,
Fabian Klötzl,
Nick Morrott,
Danny Edel,
Michael Moll,
Marcin Dulak et
Chris Kuehl
dans le projet !</p>



<toc-add-entry name="rcstats">Release-Critical bugs statistics for the upcoming release</toc-add-entry>

## release= codename for the current testing release
## url= link to the Tolimar's blog post → http://www.schmehl.info/Debian/rc-stats/7.0-wheezy/201#-##
## testing= number of bugs as defined by this UDD query → http://ur1.ca/8p8k0 (for the not shortened url, look at webwml/english/template/debian/projectnews/boilerplates.wml on the webwml repository)
## tobefixed: number of bugs as defined byt this UDD query → http://ur1.ca/8p8lo
## example:
# <rcstatslink release="Wheezy"
# 	url="http://www.schmehl.info/Debian/rc-stats/7.0-wheezy/2012-23"
# 	testing="734"
# 	tobefixed="496" />
#
# In case Tolimar's blog post has not been published this week, one can use the shorten form:
# /!\ s/rcstatslink/rcstats/
#
# <rcstats release="Wheezy"
# 	testing="613"
# 	tobefixed="410" />

<rcstatslink release="Wheezy"
	url="http://www.schmehl.info/Debian/rc-stats/7.0-wheezy/2012-##"
	testing="XXX"
	tobefixed="XXX" />


<toc-add-entry name="dsa">Annonces de sécurité Debian importantes</toc-add-entry>

	<p>L'équipe de sécurité de Debian a publié récemment des annonces
	concernant (entre autres) les paquets
<a href="$(HOME)/security/2015/dsa-3309">tidy</a>,
<a href="$(HOME)/security/2015/dsa-3310">freexl</a>,
<a href="$(HOME)/security/2015/dsa-3311">mariadb-10.0</a>,
<a href="$(HOME)/security/2015/dsa-3312">cacti</a>,
<a href="$(HOME)/security/2015/dsa-3313">linux</a>,
<a href="$(HOME)/security/2015/dsa-3314">typo3-src</a>,
<a href="$(HOME)/security/2015/dsa-3315">chromium-browser</a>,
<a href="$(HOME)/security/2015/dsa-3316">openjdk-7</a>,
<a href="$(HOME)/security/2015/dsa-3317">lxc</a>,
<a href="$(HOME)/security/2015/dsa-3318">expat</a>,
<a href="$(HOME)/security/2015/dsa-3319">bind9</a>,
<a href="$(HOME)/security/2015/dsa-3320">openafs</a>,
<a href="$(HOME)/security/2015/dsa-3321">xmltooling</a>,
<a href="$(HOME)/security/2015/dsa-3322">ruby-rack</a>,
<a href="$(HOME)/security/2015/dsa-3323">icu</a>,
<a href="$(HOME)/security/2015/dsa-3324">icedove</a>,
<a href="$(HOME)/security/2015/dsa-3325">apache2</a>,
<a href="$(HOME)/security/2015/dsa-3326">ghostscript</a>,
<a href="$(HOME)/security/2015/dsa-3327">squid3</a>,
<a href="$(HOME)/security/2015/dsa-3328">wordpress</a>,
<a href="$(HOME)/security/2015/dsa-3329">linux</a>,
<a href="$(HOME)/security/2015/dsa-3330">activemq</a>,
<a href="$(HOME)/security/2015/dsa-3331">subversion</a>,
<a href="$(HOME)/security/2015/dsa-3332">wordpress</a>,
<a href="$(HOME)/security/2015/dsa-3333">iceweasel</a>,
<a href="$(HOME)/security/2015/dsa-3334">gnutls28</a>,
<a href="$(HOME)/security/2015/dsa-3335">request-traquer4</a>
<a href="$(HOME)/security/2015/dsa-3336">nss</a>,
<a href="$(HOME)/security/2015/dsa-3337">gdk-pixbuf</a>,
<a href="$(HOME)/security/2015/dsa-3338">python-django</a>,
<a href="$(HOME)/security/2015/dsa-3339">openjdk-6</a>,
<a href="$(HOME)/security/2015/dsa-3340">zendframework</a>,
<a href="$(HOME)/security/2015/dsa-3341">conntrack</a> et
<a href="$(HOME)/security/2015/dsa-3342">vlc</a>.
	Veuillez les lire attentivement et prendre les mesures appropriées.</p>

#        <p>Debian's Backports Team released advisories for these packages:
#<a href="https://lists.debian.org/"></a>,
#        Please read them carefully and take the proper measures.</p>

	<p>L'équipe de Debian en charge du suivi à long terme de
Squeeze a publié des annonces de sécurité concernant les paquets :
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00011.html">tidy</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00012.html">inspircd</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00013.html">groovy</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00014.html">ruby1.9.1</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00015.html">libidn</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00016.html">cacti</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00017.html">cacti</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00018.html">python-tornado</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00019.html">lighttpd</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00020.html">ghostscript</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00021.html">expat</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00022.html">icu</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00023.html">bind9</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00024.html">apache2</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/07/msg00025.html">squid3</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00000.html">ia32-libs</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00001.html">openssh</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00002.html">remind</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00003.html">xmltooling</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00004.html">opensaml2</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00005.html">libidn</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00006.html">subversion</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00007.html">libstruts1.2-java</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00008.html">wordpress</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00009.html">conntrack</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00010.html">extplorer</a>,
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00011.html">wesnoth-1.8</a> et
<a href="https://lists.debian.org/debian-lts-announce/2015/08/msg00012.html">roundup</a>.
        Veuillez les lire attentivement et prendre les mesures appropriées.</p>

        <p>L'équipe en charge de la publication stable a publié des annonces de
	sécurité concernant les paquets :
<a href="https://lists.debian.org/debian-stable-announce/2015/08/msg00000.html">tzdata</a> et
<a href="https://lists.debian.org/debian-stable-announce/2015/08/msg00001.html">libdatetime-timezone-perl</a>.
        Veuillez les lire attentivement et prendre les mesures appropriées.</p>

<p>
Veuillez noter que ce sont uniquement les annonces les plus importantes 
des dernières semaines. Si vous désirez être au courant des dernières
annonces de l'équipe de sécurité de Debian, inscrivez-vous à la <a
href="https://lists.debian.org/debian-security-announce/">liste de
diffusion correspondante</a> (ainsi qu'à la <a
href="https://lists.debian.org/debian-backports-announce/">liste de
diffusion spécifique aux rétroportages</a>, celle des <a
href="https://lists.debian.org/debian-stable-announce/">mises
à jour de stable</a> et celle des <a href="https://lists.debian.org/debian-lts-announce/">mises
à jour de sécurité de la prise en charge à long terme)</a>)
</p>


<toc-add-entry name="nnwp">Nouveaux paquets dignes d'intérêt</toc-add-entry>

<p>
XXX paquets ont été ajoutés récemment à l'archive <q>unstable</q> de Debian.

	<a href="https://packages.debian.org/unstable/main/newpkg">\
	Parmi bien d'autres</a>, en voici une courte sélection :
	</p>
<ul>
<li><a href="https://packages.debian.org/unstable/main/XXX">\
	XXXX &mdash; XXXX</a></li>
</ul>


<toc-add-entry name="wnpp">Paquets qui ont besoin de travail</toc-add-entry>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2015/08/msg00200.html"
	orphaned="673"
	rfa="176" />

<toc-add-entry name="continuedpn">Continuer à lire les Nouvelles du projet Debian</toc-add-entry>

<continue-dpn />

#use wml::debian::projectnews::footer editor="Cédric Boutillier, Jean-Pierre Giraud, Martin Michlmayr, Donald Norwood, Justin B Rye, Paul Wise"
# Translators may also add a translator="foo, bar, baz" to the previous line
