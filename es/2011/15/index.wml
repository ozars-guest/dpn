#use wml::debian::projectnews::header PUBDATE="2011-12-12" SUMMARY=""
#use wml::debian::translation-check translation="1.1" maintainer="Fernando C. Estrada"

# $Id: index.wml-template 2569 2011-08-15 02:29:50Z fcestrada-guest $
# $Rev: 2569 $

<p>Le damos la bienvenida a la 1ª edición de este año del boletín de
las noticias del proyecto Debian (DPN), el boletín informativo de la
comunidad Debian. Los temas tratados en esta publicación incluyen:</p>
<toc-display/>

<toc-add-entry name="debian-edu">Debian Edu/Skolelinux 6.0.3 beta2 liberado</toc-add-entry>

<p>
Petter Reinholdtsen anunció el <a
href="http://lists.debian.org/debian-edu/2012/01/msg00129.html">lanzamiento
de Debian Edu Squeeze 6.0.3 beta2</a>: <a
href="http://wiki.debian.org/DebianEdu/Documentation/Squeeze/Installation">instrucciones de 
descarga e instalación</a> están disponibles
en la wiki, y un <a
href="http://wiki.debian.org/DebianEdu/Documentation/Squeeze/GettingStarted">capítulo muy útil 
<q>Iniciando</q></a> en el que puedes encontrar explicaciones sobre como ingresar por primera vez.
Comentarios y reportes sobre instalación pueden ser enviados a la <a 
href="http://lists.debian.org/debian-edu/">lista de correos de Debian Edu</a>.
</p>
<p>
Debian Edu es un proyecto con el objetivo de crear una mezcla de Debian 
con propósitos educativos, que puede ser usada en escuelas y otras 
instituciones educativas. The Debian Edu project develops and maintains <a
href="http://www.skolelinux.org/">Skolelinux</a>, una solución <q>out-of-the-box</q> 
completa y libre para escuelas.
Para más información sobre Debian Edu, porfavor visita la
<a href="http://wiki.debian.org/DebianEdu">wiki</a>.
</p>

<toc-add-entry name="javarm">XXX</toc-add-entry>

<p>Más información</p>


<toc-add-entry name="sdl">XX</toc-add-entry>

<p></p>

<toc-add-entry name="dpl">XXX</toc-add-entry>


<p></p>

<toc-add-entry name="ubuapprdeb">XXX</toc-add-entry>

<p></p>


<toc-add-entry name="indiamangalore">XXX</toc-add-entry>

<p></p>


<toc-add-entry name="mirror">Nueva réplica en El Salvador</toc-add-entry>

<p>El equipo de réplicas de Debian anuncia la primer réplica en <a
href="http://ftp.sv.debian.org/debian/">El Salvador</a>, patrocinado por el 
<a href="http://www.salud.gob.sv/">Ministerio de Salud</a>, con la ayuda de 
René Mayorga Carlos Juan Martín Pérez. Se les invita a los usuarios de Debian 
en El Salvador a actualizar su <code>/etc/apt/sources.list</code>  para usar
<code>ftp.sv.debian.org</code>. 

Carlos Juan Martin Pérez nos dice, "para nosotros, el Ministerio de Salud, y como miembro de la comunidad
de Software Libre de El Salvador, es un honor pertenecer a la gran familia
Debian".

Para otros países, la <a href="$(HOME)/mirror/list">lista completa de réplicas</a> 
está disponible en linea. Todavía hay muchos países carentes de buena conectividad
hacia una réplica de Debian; patrocinadores interesados en alojar una réplica, están
invitados a contactar al equipo de réplicas de Debian.</p>


<toc-add-entry name="debexpo">Mantenedores de Debexpo hacen el llamado para contribuciones</toc-add-entry>

<p>Discutiendo el potencial de integración de los equipos de empaquetamiento 
en Debexpo, el software detrás del servicio <a href="http://mentors.debian.net/">mentors.debian.net</a>,
Arno Töll emitió un <a href="http://lists.debian.org/4ECD5330.40104@toell.net">llamado para contribuciones</a>,
ya que los miembros del actual equipo se encuentran ocupados.</p>

<p><a href="http://wiki.debian.org/Debexpo">Debexpo</a> es mantenido como un
<a href="http://alioth.debian.org/projects/debexpo/">proyecto en Alioth</a> 
</p>

<toc-add-entry name="bsp">Bug Squashing Party marathon started</toc-add-entry>


<toc-add-entry name="fosdem-cft">Llamado para charlas: FOSDEM 2012</toc-add-entry>

<p>
Wouter Verhelst envió un <a href="http://lists.debian.org/20111111113115.GZ17352@grep.be">llamado para charlas
para el salón de desarrolladores de distribuciones</a> en el próximo
<a href="http://www.debian.org/events/2012/0204-fosdem">FOSDEM 2012</a>, la reunión
Europea de desarrolladores de Software Libre y Código Abierto.
FOSDEM se llevará a cabo a inicio de febrero 2012 en Bruselas, Bélgica.
Las sesiones pueden ser en distinto formato, incluyendo charlas,
sesiones BoF y mesas redondas. Two cross-distributions devrooms are intended to be
for people from any participating distribution project, and may cover
Debian-specific subjects, or indeed targeted at Debian developers only.
</p>


<toc-add-entry name="s390">New s390 buildd at Karlsruhe Institute of Technology</toc-add-entry>


<toc-add-entry name="twid">Más entrevistas</toc-add-entry>
<p>Han habido 3 nuevas entrevistas sobre las <q>Personas detrás de
Debian</q>, las cuales permiten conocer más acerca de: 
<a href="http://www.perrier.eu.org/weblog/2011/11/05#interview-raphael">Raphaël
Hertzog</a>, mantenedor de dpkg, escritor de libros; 
<a href="http://raphaelhertzog.com/2011/11/17/people-behind-debian-mark-shuttleworth-ubuntus-founder/">Mark
Shuttleworth</a>, Fundador de Ubuntu; y 
<a href="http://raphaelhertzog.com/2011/11/22/people-behind-debian-stefano-zacchiroli-debian-project-leader/">Stefano
Zacchiroli</a>, Lider del Proyecto Debian.

<p>Stefano Zacchiroli también fue entrevistado por Karen Sandler
<a href="http://faif.us/cast/2011/nov/29/0x1D/">en el oggcast de <acronym lang="en"
title="Free as in Freedom">FaiF</acronym></a>, por Amber Granger durante
<a href="http://www.youtube.com/watch?v=p64OUfcfe5M">el Ubuntu Developer Summit</a> y
<a href="http://interview.lici.it/2011/11/15/an-interview-for-linux-to-stefano-zacchiroli/">por
<acronym lang="en" title="Linux Certification Institute">Lici.it</acronym></a>
(<a href="http://interview.lici.it/it/2011/11/15/an-interview-for-linux-to-stefano-zacchiroli/">versión original en Italian</a>).
Adicionalmente el <a href="http://neuro.debian.net/">equipo de NeuroDebian</a> fue
<a href="http://www.floss4science.com/interview-neurodebian/">entrevistado</a>
por "FLOSS for Science".</p>


<toc-add-entry name="newcontributors">Nuevos colaboradores de
Debian</toc-add-entry>

<p>4 aspirantes han <a
href="https://nm.debian.org/nmlist.php#newmaint">sido
aceptados</a> como desarrolladores de Debian, 13 aspirantes han sido
aceptados como mantenedores de Debian, y 25 personas han <a
href="http://udd.debian.org/cgi-bin/new-maintainers.cgi">comenzado
a mantener paquetes</a> desde la edición anterior de las noticias del
proyecto Debian. Demos una cordial bienvenida a 
Nicholas Breen,
Vincent Legout,
Antoine Beaupré,
Gergely Nagy,

Wolodja Wentland,
Vasudev Kamath,
Matthias Klumpp,
José Manuel Santamaría Lema,
Floris Bruynooghe,
Cédric Boutillier,
Christophe Trophime,
Tobias Hansen,
Nicolas Dandrimont,
Simone Rosetto,
Jonas Genannt,
Laszlo Kajan,
James Page,

Mikolaj Izdebski,
Félix Arreola Rodríguez,
Henry Velez,
Gastón Ramos,
Stephen M. Webb,
Miguel de Val Borro,
Simon Chopin,
Paolo Greppi,
B. Clausius,
Mateusz Kijowski,
José Luis Segura Lucas,
Marcin Kulisz (kuLa),
Teus Benschop,
Ole Streicher,
Paolo Rotolo,
Martin Erik Werner,
Raoul Gunnar Borenius,
Geoffrey Thomas,
Alkis Georgopoulos,
Jerome Kieffer,
Christopher Gervais,
Håkon Nessjøen,
David Stone,
Nicolas Bourdaud
y
Mathias Ertl
	a nuestro proyecto.</p>

<toc-add-entry name="rcstats">Estadísticas sobre los fallos con
severidad crítica para la publicación de la próxima versión
estable</toc-add-entry>

<p>De acuerdo a la <a href="http://udd.debian.org/bugs.cgi"> interfaz de
búsqueda de fallos en la base de datos «Ultimate» de Debian</a>, la
próxima versión 7.0 de Debian llamada <q>Wheeezy</q>, está actualmente
afectada por 1005 fallos con severidad crítica. Ignorando fallos que son
fáciles de resolver o que están en camino de ser resueltos, se requiere
resolver cerca de 849 fallos con severidad crítica para que sea posible
la publicación de la próxima versión estable.</p>

<p>Hay también <a
href="http://blog.schmehl.info/Debian/rc-stats/7.0-wheezy/2011-49">estadísticas
con mayor detalle</a> así como algunas <a
href="http://wiki.debian.org/ProjectNews/RC-Stats">sugerencias sobre
cómo interpretar</a> estas cifras.</p>

<toc-add-entry name="dsa">Anuncios importantes del equipo de seguridad de Debian</toc-add-entry>
	
<p>Moritz Muehlenhoff ha recordado a los usuarios que el <a
href="http://lists.debian.org/debian-security-announce/2011/msg00238.html">soporte para seguridad
</a> para Debian GNU/Linux 5.0 <q>Lenny</q> finalizará el 6 de febrero de 2012.</p>
	
<p>El equipo de seguridad de Debian ha publicado recientemente avisos 
relacionados (entre otros) a los siguientes paquetes: 
<a href="$(HOME)/security/2011/dsa-2338">moodle</a>,
<a href="$(HOME)/security/2011/dsa-2339">nss</a>,
<a href="$(HOME)/security/2011/dsa-2336">ffmpeg</a>,
<a href="$(HOME)/security/2011/dsa-2340">postgresql</a>,
<a href="$(HOME)/security/2011/dsa-2341">iceweasel</a>,
<a href="$(HOME)/security/2011/dsa-2342">iceape</a>,
<a href="$(HOME)/security/2011/dsa-2343">openssl</a>,
<a href="$(HOME)/security/2011/dsa-2344">python-django-piston</a>,
<a href="$(HOME)/security/2011/dsa-2345">icedove</a>,
<a href="$(HOME)/security/2011/dsa-2346">proftpd-dfsg</a>,
<a href="$(HOME)/security/2011/dsa-2347">bind9</a>,
<a href="$(HOME)/security/2011/dsa-2349">spip</a>,
<a href="$(HOME)/security/2011/dsa-2350">freetype</a>,
<a href="$(HOME)/security/2011/dsa-2348">systemtap</a>,
<a href="$(HOME)/security/2011/dsa-2351">wireshark</a>,
<a href="$(HOME)/security/2011/dsa-2352">puppet</a>,
<a href="$(HOME)/security/2011/dsa-2353">ldns</a>,
<a href="$(HOME)/security/2011/dsa-2354">cups</a>,
<a href="$(HOME)/security/2011/dsa-2355">clearsilver</a>,
<a href="$(HOME)/security/2011/dsa-2356">openjdk-6</a>,
<a href="$(HOME)/security/2011/dsa-2357">evince</a>,
<a href="$(HOME)/security/2011/dsa-2358">openjdk-6</a> (for <q>Lenny</q>),
<a href="$(HOME)/security/2011/dsa-2359">mojarra</a>,
<a href="$(HOME)/security/2011/dsa-2361">chasen</a> and
<a href="$(HOME)/security/2011/dsa-2362">acpid</a>.
Léalos cuidadosamente y tome las medidas pertinentes.</p>
	
<p>El equipo «Backports» de Debian publicó avisos para los siguientes paquetes:
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00000.html">libvirt</a>,
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00001.html">libreoffice</a>,
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00002.html">puppet</a>,
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00003.html">iceweasel</a>,
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00004.html">openssl</a>,
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00005.html">apache2</a>,
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00006.html">libsndfile</a> and
<a href="http://lists.debian.org/debian-backports-announce/2011/11/msg00007.html">nss</a>.
Léalos cuidadosamente y tome las medidas pertinentes.</p>
	
<p>El equipo «Release» encargado de la versión estable de Debian ha
publicado aviso para el siguiente paquete:
<a href="http://lists.debian.org/debian-stable-announce/2011/11/msg00001.html">linux-2.6</a>.
Léalos cuidadosamente y tome las medidas pertinentes.</p>
	
<p>Esto es una selección de los avisos de seguridad más importantes en
las últimas semanas. Si necesita mantenerse al día sobre los avisos de
seguridad publicados por el equipo de seguridad de Debian debe
suscribirse a la lista de correo de<a
href="http://lists.debian.org/debian-security-announce/">anuncios de
seguridad</a>, además de la lista de <a
href="http://lists.debian.org/debian-backports-announce/">anuncios
de «Backports»</a>, la lista de <a
href="http://lists.debian.org/debian-stable-announce/">anuncios sobre la
versión estable</a>, y la lista de <a
href="http://lists.debian.org/debian-volatile-announce/">anuncios de
«volatile»</a> para Debian «Lenny», (la distribución estable
anterior «oldstable»).</p>


<toc-add-entry name="nnwp">Nuevos paquetes</toc-add-entry>

<p><a href="http://packages.debian.org/unstable/main/newpkg">761
paquetes</a> se han añadido recientemente al archivo de la versión
inestable («unstable») de Debian, entre ellos se encuentran:</p> 

<ul>
<li><a href="http://packages.debian.org/unstable/main/4digits">\
4digits &mdash; guess-the-number game, aka Bulls and Cows</a></li>
<li><a href="http://packages.debian.org/unstable/main/guacamole">\
guacamole &mdash; HTML5 web application for accessing remote desktops</a></li>
<li><a href="http://packages.debian.org/unstable/main/knot">\
knot &mdash; authoritative domain name server</a></li>
<li><a href="http://packages.debian.org/unstable/main/mgen">\
mgen &mdash; packet generator for IP network performance tests</a></li>
<li><a href="http://packages.debian.org/unstable/main/minetest">\
minetest &mdash; InfiniMiner/Minecraft-inspired open game world</a></li>
<li><a href="http://packages.debian.org/unstable/main/musique">\
musique &mdash; simple but sophisticated graphical music player</a></li>
<li><a href="http://packages.debian.org/unstable/main/pinpoint">\
pinpoint &mdash; hacker-friendly presentation program</a></li>
<li><a href="http://packages.debian.org/unstable/main/r8168-dkms">\
r8168-dkms &mdash; dkms source for the r8168 network driver</a></li>
<li><a href="http://packages.debian.org/unstable/main/redsocks">\
redsocks &mdash; redirect any TCP connection to a SOCKS or HTTPS proxy server</a></li>
<li><a href="http://packages.debian.org/unstable/main/uhub">\
uhub &mdash; high performance Advanced Direct Connect p2p hub</a></li>
</ul>

<toc-add-entry name="wnpp">Paquetes para los que se solicita ayuda</toc-add-entry>
<p><a href="$(DEVEL)/wnpp/orphaned">\
400 paquetes están huérfanos</a> y <a href="$(DEVEL)/wnpp/rfa">\
145 paquetes se han puesto en adopción</a>. Revise la lista completa de
<a href="$(DEVEL)/wnpp/help_requested">paquetes que requieren ayuda</a>.</p>

<toc-add-entry name="continuedpn">¿Desea continuar leyendo las noticias del proyecto Debian?</toc-add-entry> 

<p>Ayúdenos en la elaboración de este boletín. Necesitamos más
voluntarios que informen y/o escriban sobre lo que acontece en la
comunidad del proyecto Debian, así como ayuda en la traducción del
boletín. Revise la <a
href="http://wiki.debian.org/ProjectNews/HowToContribute">página sobre
cómo contribuir</a> para obtener mayor información sobre cómo ayudar.
Esperamos sus comentarios en la dirección de correo electrónico
<a href="mailto:debian-publicity@lists.debian.org">debian-publicity@lists.debian.org</a>.</p>


#use wml::debian::projectnews::footer editor="XXX, Francesca Ciceri, Jeremiah C. Foster, David Prévot, Alexander Reichle-Schmehl, Alexander Reshetov, Justin B Rye" translator="Fernando c. Estrada"

